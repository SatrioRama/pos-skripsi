<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('shift_id')->nullable();
            $table->string('transaction_number')->nullable();
            $table->integer('payment_method')->nullable();
            $table->string('approval_code')->nullable();
            $table->string('voucher_id')->nullable();
            $table->double('total')->nullable();
            $table->double('discount')->nullable();
            $table->double('total_amount')->nullable();
            $table->double('cash')->nullable();
            $table->double('change')->nullable();
            $table->integer('ticket_type')->nullable();
            $table->double('cash_back')->nullable();
            $table->double('refund')->nullable();
            $table->string('user_id')->nullable();
            $table->string('kasir_refund_id')->nullable();
            $table->string('store_refund_id')->nullable();
            $table->integer('store_branch_id')->nullable();
            $table->text('description')->nullable();
            $table->boolean('struk_print')->default(false);
            $table->boolean('ticket_print')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
