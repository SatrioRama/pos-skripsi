<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shifts', function (Blueprint $table) {
            $table->id();
            $table->string('user_id')->nullable();
            $table->integer('store_branch_id')->nullable();
            $table->dateTime('open_at')->nullable();
            $table->dateTime('close_at')->nullable();
            $table->double('begining_cash')->nullable();
            $table->double('total_transaction')->nullable();
            $table->double('transaction_cash')->nullable();
            $table->double('transaction_edc')->nullable();
            $table->double('expected_cash')->nullable();
            $table->double('actual_cash')->nullable();
            $table->double('difference')->nullable();
            $table->double('tip')->nullable();
            $table->double('refund')->nullable();
            $table->text('note')->nullable();
            $table->boolean('active')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shifts');
    }
}
