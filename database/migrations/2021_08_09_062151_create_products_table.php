<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('name')->nullable();
            $table->double('price')->nullable();
            $table->double('holiday_price')->nullable();
            $table->double('promo_price')->nullable();
            $table->integer('product_type_id')->nullable();
            $table->string('day_active')->nullable();
            $table->integer('timer')->nullable();
            $table->string('image')->nullable();
            $table->date('start_date_promo')->nullable();
            $table->date('end_date_promo')->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
