<?php

namespace Database\Seeders;

use App\Models\StoreBranch;
use Illuminate\Database\Seeder;

class StoreBranchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $alcrace = StoreBranch::create([
            'name' => 'ALC-Race Sukabumi',
            'address' => 'Jl. A. Yani No.121-127, Kec. Cikole',
            'city' => 'Sukabumi',
            'telephone' => '021-326598',
            'store_branch_type_id' => '2',
        ]);
        $cafe = StoreBranch::create([
            'name' => 'Bencoolen Sukabumi',
            'address' => 'Jl. A. Yani No.121-127, Kec. Cikole',
            'city' => 'Sukabumi',
            'telephone' => '021-326598',
            'store_branch_type_id' => '1',
        ]);
    }
}
