<?php

namespace Database\Seeders;

use App\Models\StoreBranchType;
use Illuminate\Database\Seeder;

class StoreBranchTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cafe = StoreBranchType::create([
            'name' => 'Cafe',
        ]);
        $playground = StoreBranchType::create([
            'name' => 'Playground',
        ]);
    }
}
