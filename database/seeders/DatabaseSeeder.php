<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        // $this->call(ProductTableSeeder::class);
        $this->call(ProductTypeTableSeeder::class);
        // $this->call(VehicleTableSeeder::class);
        // $this->call(StoreBranchTableSeeder::class);
        $this->call(StoreBranchTypeTableSeeder::class);
        $this->call(PaymentMethodSeeder::class);
    }
}
