<div class="card no-padding no-border">
    <div class="card-header">
        <h3><i class='nav-icon fa fa-shopping-cart'></i> Shopping Cart</h3>
    </div>
    <div class="card-body">
        <form action="{{route('transaction.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            @livewire('list-cart')
        </div>
        <div class="card-footer">
            @livewire('discount')
            @livewire('cart-money')
            <input type="hidden" name="shift_id" class="form-control" aria-label="Sizing example input" value="{{$shift->id}}" aria-describedby="shift_id">
            <input type="hidden" name="user_id" class="form-control" aria-label="Sizing example input" value="{{backpack_user()->id}}" aria-describedby="user_id">
            <input type="hidden" name="store_branch_id" class="form-control" aria-label="Sizing example input" value="{{$store->id}}" aria-describedby="store_branch_id">
            <div class="input-group mb-3">
                <span class="input-group-text col-md-3" id="payment_method">Payment Type</span>
                <select name="payment_method" class="form-select col-md-9" aria-label="Default select example">
                    @foreach (backpack_user()->storeBranch->first()->paymentMethod as $payment)
                        <option value="{{@$payment->id}}">{{@$payment->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text col-md-3" id="approval_code">Approval Code</span>
                <input id="approval_code" type="text" style="text-transform:uppercase" name="approval_code" class="form-control" aria-label="Sizing example input" aria-describedby="approval_code">
            </div>
            @livewire('voucher')
            <div class="input-group mb-3">
                <span class="input-group-text col-md-3" id="cash">Cash</span>
                <span class="input-group-text col-md-1" id="cash">Rp</span>
                <input id="cash" onkeypress="getChange()" type="number" name="cash" class="form-control" aria-label="Sizing example input" aria-describedby="cash">
            </div>
            <div class="input-group mb-3">
                <button type="submit" class="btn btn-success" style="width: 100%">SUBMIT</button>
            </div>
        </form>
    </div>
    </div>
</div>
