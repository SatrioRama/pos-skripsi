<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
        <style>
            @media print {
                #printPageButton {
                    display: none;
                }
                #backButton {
                    display: none;
                }
            }
        </style>
        <title>{{$data->transaction_number}}</title>
    </head>
    <body>

    <button id="backButton" onClick="window.history.back();" style="width: 100%"><i class="fa fa-print fa-fw"></i> <strong>KEMBALI</strong></button>
    <br>
    <br>
    <button id="printPageButton" onClick="window.print();" style="width: 100%"><i class="fa fa-print fa-fw"></i> <strong>PRINT</strong></button>

    <table cellspacing="0" border="0" style="width: 100%">
            <tr>
                <td style="font-size: 200%"><center>{{$data->storebranch->name}}</center></td>
            </tr>
            <tr>
                <td><center>{{$data->storebranch->address}}</center></td>
            </tr>
            <tr>
                <td><center>{{$data->storebranch->city}}</center></td>
            </tr>
        </table>
        <table cellspacing="0" border="0" style="width: 100%">
            <tr>
                <td style="width: 50%">Receipt Number</td>
                <td style="width: 1%">:</td>
                <td style="width: 49%">{{$data->transaction_number}}</td>
            </tr>
            <tr>
                <td>Cashier</td>
                <td>:</td>
                <td>{{$data->user->name}}</td>
            </tr>
        </table>
        <table cellspacing="0" border="0" style="width: 100%">
            <tr>
                <td style="text-align: left">{{date('d-m-Y',strtotime($data->created_at))}}</td>
                <td style="text-align: right">{{date('H:i:s',strtotime($data->created_at))}}</td>
            </tr>
        </table>
        <hr>
        <table cellspacing="0" border="0" style="width: 100%">
            <tr>
                <th style="width: 35%">Item</th>
                <th style="width: 5%">Qty</th>
                <th style="width: 30%">Price</th>
                <th style="width: 30%">Total</th>
            </tr>
            @foreach ($data->details as $item)
                <tr>
                    <td>{{$item->product->name}}</td>
                    <td style="text-align: center">{{number_format($item->qty)}}</td>
                    <td>Rp{{number_format($item->price, 0, ',', '.')}}</td>
                    <td>Rp{{number_format($item->sub_total, 0, ',', '.')}}</td>
                </tr>
            @endforeach
        </table>
        <hr>
        <table cellspacing="0" border="0" style="width: 95%; text-align: right">
            <tr>
                <td style="width: 59%">Subtotal</td>
                <td style="width: 1%">:</td>
                <td style="width: 40%">Rp{{number_format($data->total, 0, ',', '.')}}</td>
            </tr>
            <tr>
                <td>Discount</td>
                <td>:</td>
                <td>Rp{{number_format($data->discount, 0, ',', '.')}}</td>
            </tr>
            <tr>
                <td>Total</td>
                <td>:</td>
                <td>Rp{{number_format($data->total_amount, 0, ',', '.')}}</td>
            </tr>
            <tr>
                <td>Cash</td>
                <td>:</td>
                <td>Rp{{number_format($data->cash, 0, ',', '.')}}</td>
            </tr>
            <tr>
                <td>Change</td>
                <td>:</td>
                <td>Rp{{number_format($data->change, 0, ',', '.')}}</td>
            </tr>
            @if (isset($data->refund))
                <tr>
                    <td>Refund</td>
                    <td>:</td>
                    <td>Rp{{number_format(($data->refund*-1), 0, ',', '.')}}</td>
                </tr>
            @endif
            @if (isset($data->approval_code))
                <tr>
                    <td>Approval Code</td>
                    <td>:</td>
                    <td>{{$data->approval_code}}</td>
                </tr>
            @endif
        </table>
        <br>
        <table cellspacing="0" border="0" style="width: 100%; text-align: center">
            <tr>
                <td>
                    No Refund/Exchange
                    <br>
                    * * Thank You * *
                </td>
            </tr>
            {{-- <tr>
                <td><i class="las la-globe-americas"></i> www.alcrace.com</td>
            </tr>
            <tr>
                <td><i class="lab la-instagram"></i> &commat;alcraceofficial</td>
            </tr> --}}
        </table>
    </body>
</html>

