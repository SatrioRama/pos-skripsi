@extends(backpack_view('blank'))

@php
  $defaultBreadcrumbs = [
    trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
    $crud->entity_name_plural => url($crud->route),
    trans('backpack::crud.preview') => false,
  ];

  // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
  $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
@endphp

@section('header')
	<section class="container-fluid d-print-none">
    	<a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
		<h2>
	        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
	        <small>{!! $crud->getSubheading() ?? mb_ucfirst(trans('backpack::crud.preview')).' '.$crud->entity_name !!}</small>
	        @if ($crud->hasAccess('list'))
	          <small class=""><a href="{{ url($crud->route) }}" class="font-sm"><i class="la la-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
	        @endif
	    </h2>
    </section>
@endsection

@section('content')

<div class="row">
	<div class="col-md-12">

	<!-- Default box -->
	  <div class="">
	  	@if ($crud->model->translationEnabled())
	    <div class="row">
	    	<div class="col-md-12 mb-2">
				<!-- Change translation button group -->
				<div class="btn-group float-right">
				  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    {{trans('backpack::crud.language')}}: {{ $crud->model->getAvailableLocales()[request()->input('locale')?request()->input('locale'):App::getLocale()] }} &nbsp; <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				  	@foreach ($crud->model->getAvailableLocales() as $key => $locale)
					  	<a class="dropdown-item" href="{{ url($crud->route.'/'.$entry->getKey().'/show') }}?locale={{ $key }}">{{ $locale }}</a>
				  	@endforeach
				  </ul>
				</div>
			</div>
	    </div>
	    @else
	    @endif
	    <div class="card no-padding no-border">
            <div class="card-header">
                <form action="{{ route('transaction.update', $crud->entry->id) }}" method="post" name="form_add_vehicle" id="form_add_vehicle">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="http_referrer" value="{{backpack_url('ticket/'.$crud->entry->id.'/show')}}">
                    <input type="hidden" name="product_id" value="{{ $crud->entry->id }}">

                    @if (isset($crud->entry->details))
                    <table id="tableTransaction" class="table table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Qty</th>
                                <th class="text-center">Qty Refund</th>
                                <th class="text-center">Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($crud->entry->details as $key => $detail)
                                <tr>
                                    <td class="text-center">{{$key+1}}</td>
                                    <td class="text-center">{{$detail->product->name}}</td>
                                    @if ($detail->qty >= 0)
                                        <td class="text-center">{{$detail->qty}}</td>
                                        <td class="text-center">
                                            <input type="hidden" name="id[]" class="form-control" aria-label="Default" aria-describedby="refund" value="{{$detail->id}}">
                                            <input type="number" name="qty[]" class="form-control" style="width: 100%" aria-label="Default" aria-describedby="refund">
                                        </td>
                                    @else
                                        <td class="text-center" colspan="2">{{$detail->qty}}</td>
                                    @endif
                                    <td class="text-center">Rp{{number_format($detail->price, 2, ',', '.')}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                    <br>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Description</span>
                        </div>
                        <textarea name="description" class="form-control" aria-label="With textarea" required>{{@$crud->entry->description}}</textarea>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text">Username Store Manager</span>
                        <input type="text" class="form-control" name="username_store_manager" placeholder="Username Store Manager" aria-label="Username" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text">Password Store Manager</span>
                        <input type="password" class="form-control" name="password_store_manager" placeholder="Password Store Manager" aria-label="Server" required>
                    </div>
                    <input type="hidden" name="save_action" value="save_and_back">
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-primary" id="add-buton-out">SUBMIT</button>
                    </div>
                </form>
            </div>
	    </div>
	  </div>
	</div>
</div>
@endsection

@section('after_styles')
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css') }}">
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css">
@endsection

@section('after_scripts')
	<script src="{{ asset('packages/backpack/crud/js/crud.js') }}"></script>
	<script src="{{ asset('packages/backpack/crud/js/show.js') }}"></script>
@endsection
