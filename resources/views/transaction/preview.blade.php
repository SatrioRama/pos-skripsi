@extends(backpack_view('blank'))


@section('header')
	<section class="container-fluid d-print-none">
    	<a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
		<h2>
	        <span class="text-capitalize">Transaction</span>
	        <small>Transaction</small>
	          <small class=""><a href="{{ backpack_url('transaction') }}" class="font-sm"><i class="la la-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>transaction</span></a></small>
	    </h2>
    </section>
@endsection

@section('content')

<div class="container">
    @include('transaction.header')
    @include('transaction.detail')
</div>
@endsection

@section('after_styles')
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css') }}">
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css">
@endsection

@section('after_scripts')
	<script src="{{ asset('packages/backpack/crud/js/crud.js') }}"></script>
	<script src="{{ asset('packages/backpack/crud/js/show.js') }}"></script>
    <script>
        function printStruk(route) {
            print(route);
        }
        function printTicket(route) {
            print(route);
        }
        function print(doc) {
            var objFra = document.createElement('iframe');   // CREATE AN IFRAME.
            objFra.style.visibility = "hidden";    // HIDE THE FRAME.
            objFra.src = doc;                      // SET SOURCE.
            document.body.appendChild(objFra);  // APPEND THE FRAME TO THE PAGE.
            objFra.contentWindow.focus();       // SET FOCUS.
            objFra.contentWindow.print();      // PRINT IT.
        }
    </script>
@endsection
