<!-- This file is used to store topbar (right) items -->
@if (backpack_user()->hasRole('kasir'))
    @php
        $shift_session = \App\Models\Shift::where('user_id', '=', @backpack_user()->id)->where('store_branch_id', '=', @backpack_user()->storeBranch->first()->id)->where('active', '=', App\Status::ACTIVE)->first();
    @endphp
    @if (backpack_user()->hasRole('kasir') && count(backpack_user()->storeBranch) == 1 && !empty($shift_session))
    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#finishShift">
        <i class="fa fa-plus"></i> Selesai Shift
    </button>
    @include('role_dashboard.kasir.finishshift')
    @endif
@endif
