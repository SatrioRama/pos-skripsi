@extends(backpack_view('blank'))

@section('content')
@include('dashboard.filter')
@if (isset($_COOKIE['dashboard']) && $_COOKIE['dashboard'] == 'yearly')
    @include('role_dashboard.admin.yearly_chart')
@elseif (isset($_COOKIE['dashboard']) && $_COOKIE['dashboard'] == 'monthly')
    @include('role_dashboard.admin.monthly_chart')
@elseif (isset($_COOKIE['dashboard']) && $_COOKIE['dashboard'] == 'data-stock')
    @include('role_dashboard.admin.data-stock')
@elseif (isset($_COOKIE['dashboard']) && $_COOKIE['dashboard'] == 'popular')
    @include('role_dashboard.admin.popular_product')
@else
    @include('role_dashboard.admin.yearly_chart')
@endif
@endsection
@include('role_dashboard.admin.modal_detail_transactions')

@section('after_styles')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css">
@endsection

@section('after_scripts')
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
<script>
    $(document).ready(function() {
        $("#yearly").on("click", function() {
            document.cookie = "dashboard=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            document.cookie = "dashboard=yearly; expires=Thu, 01 Jan 2030 00:00:00 UTC;";
        }),

        $("#monthly").on("click", function() {
            document.cookie = "dashboard=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            document.cookie = "dashboard=monthly; expires=Thu, 01 Jan 2030 00:00:00 UTC;";
        }),

        $("#data-stock").on("click", function() {
            document.cookie = "dashboard=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            document.cookie = "dashboard=data-stock; expires=Thu, 01 Jan 2030 00:00:00 UTC;";
        }),

        $("#popular").on("click", function() {
            document.cookie = "dashboard=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            document.cookie = "dashboard=popular; expires=Thu, 01 Jan 2030 00:00:00 UTC;";
        }),

        $('table.display').DataTable( {
            dom: 'Bfrtip',
            "pageLength": -1,
            "scrollX": true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        } );
        // $('#data-stock').DataTable( {
        //     dom: 'Bfrtip',
        //     "pageLength": -1,
        //     "scrollX": true,
        //     buttons: [
        //         'copy', 'csv', 'excel', 'pdf', 'print'
        //     ]
        // } );
    } );
</script>
<script>
    function detail(store_id, date) {
        $.ajax({
            type: "post",
            url: "{{ backpack_url('Api/DetailTransaction') }}",
            data: {
                store_id: store_id,
                date: date,
                _token: '{{ csrf_token() }}'
            },
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    // modals
                    $('#modalTransactionDetails').modal('show');
                    // header tittle
                    var store_branch_label = document.getElementById("store-branch-label");
                    var date_label = document.getElementById("date-label");
                    store_branch_label.innerHTML = response.data.store_branch;
                    date_label.innerHTML = response.data.date_label;
                    // footer table
                    var total_sold_sum = document.getElementById("total-sold-sum");
                    var income_sum = document.getElementById("income-sum");
                    total_sold_sum.innerHTML = response.data.total_sold_sum;
                    income_sum.innerHTML = response.data.income_sum;
                    // body table
                    for (const [key, value] of Object.entries(response.data.product_name)) {
                        // console.log(key, value);
                        var table = document.getElementById("body-table");
                        var row = table.insertRow(0);
                        var cell1 = row.insertCell(0);
                        var cell2 = row.insertCell(1);
                        var cell3 = row.insertCell(2);
                        cell1.innerHTML = value;
                        cell2.innerHTML = response.data.total_sold[key];
                        cell3.innerHTML = response.data.income[key];
                    }
                }else{
                    swalError({
                        message: response.data.message,
                        response: response.data.error,
                    })
                }
            }
        });
    }
</script>
<script>
    $('#modalTransactionDetails').on('hidden.bs.modal', function () {
        $("#body-table").empty();
    })
</script>
@endsection
