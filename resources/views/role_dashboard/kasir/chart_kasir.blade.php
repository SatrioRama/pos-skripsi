<div class="row">
    <div class="col-md-12">
        @php
        Widget::add([
            'type'    => 'div',
            'class'   => 'row',
            'content' => [ // widgets
                ['type'       => 'chart',
                'controller' => \App\Http\Controllers\Admin\Charts\KasirChartChartController::class,
                'wrapper' => ['class' => 'col text-center'],
                'content' => ['header' => 'Penjualan'],
                ]
            ]
        ])->section('after_content');
        @endphp
    </div>
</div>
