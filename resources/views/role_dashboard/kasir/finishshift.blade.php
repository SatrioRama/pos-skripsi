<!-- Modal -->
<div class="modal fade" id="finishShift" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="finishShiftLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="finishShiftLabel">Finish Shift</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('shift.update', $shift_session->id) }}" method="post" name="finish_shift" id="finish_shift">
                    @csrf
                    @method('PUT')
                    @php
                        $transaction_jancoeg = \App\Models\Transaction::where('user_id', '=', backpack_user()->id)->where('store_branch_id', '=', backpack_user()->storeBranch->first()->id)->where('shift_id', '=', $shift_session->id);
                        $transaction = \App\Models\Transaction::where('user_id', '=', backpack_user()->id)->where('store_branch_id', '=', backpack_user()->storeBranch->first()->id)->where('shift_id', '=', $shift_session->id);
                        $total_refund = \App\Models\Transaction::whereDate('created_at', '=', date(today()))->where('store_branch_id', '=', backpack_user()->storeBranch->first()->id)->sum('refund');
                        $total_refund_check = \App\Models\Shift::whereDate('created_at', '=', date(today()))->where('store_branch_id', '=', backpack_user()->storeBranch->first()->id)->sum('refund');
                        $refund = ($total_refund - $total_refund_check) * -1;
                    @endphp
                    <input type="hidden" name="http_referrer" value="{{backpack_url('dashboard')}}">
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="begining_cash">Begining Balance</span>
                        <input type="number"  name="begining_cash" value="{{$shift_session->begining_cash}}" class="form-control" aria-label="Sizing example input" aria-describedby="begining_cash">
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="transaction_cash">Transaction Cash</span>
                        <input type="number"  name="transaction_cash" value="{{@$transaction->where('payment_method', '=', 1)->sum('total_amount') + (@$transaction->where('payment_method', '=', 1)->sum('refund') * -1)}}" class="form-control" aria-label="Sizing example input" aria-describedby="transaction_cash">
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="transaction_edc">Transaction EDC</span>
                        <input type="number"  name="transaction_edc" value="{{@$transaction_jancoeg->where('payment_method', '!=', 1)->sum('total_amount') + (@$transaction->where('payment_method', '!=', 1)->sum('refund') * -1)}}" class="form-control" aria-label="Sizing example input" aria-describedby="transaction_edc">
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="transaction_refund">Transaction Refund</span>
                        <input type="number"  name="transaction_refund" value="{{$refund}}" class="form-control" aria-label="Sizing example input" aria-describedby="transaction_refund">
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="finish_cash">Finish Cash</span>
                        <input type="number" name="finish_cash" class="form-control" aria-label="Sizing example input" aria-describedby="finish_cash" required>
                    </div>
                    <input type="hidden" name="save_action" value="save_and_back">
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-primary" id="add-buton-out">SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
