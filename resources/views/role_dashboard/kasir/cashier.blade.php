<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
              Selamat Bekerja
            </div>
            <div class="card-body">
                <h5 class="card-title">Kasir {{backpack_user()->storeBranch->first()->name}}</h5>
                <p class="card-text">Hitung jumlah saldo sebelum memulai</p>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#startShift">
                    <i class="fa fa-plus"></i> Start Cashier
                </button>
            </div>
        </div>
    </div>
</div>
