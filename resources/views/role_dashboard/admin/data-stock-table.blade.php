<div class="row">
    <div class="col-md-12">
        <div class="card border-danger col-md-12">
            {{-- <div class="card-header text-center">Data Table Stock Produk Bulan {{Carbon\Carbon::parse(request('show_dashboard_date', date('Y-m-d')))->format('M Y')}}</div> --}}
            <div class="card-header text-center">Data Table Stock Produk</div>
            <div class="card-body">
                <table id="data-stock" class="display nowrap" style="width:100%; text-align:center">
                    <thead>
                        <tr>
                            <th>Cabang</th>
                            <th>Produk</th>
                            <th>Jumlah Masuk</th>
                            <th>Jumlah Terjual</th>
                            <th>Jumlah Selisih</th>
                            <th>Jumlah Pendapatan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($admin['stock']['store'] as $store)
                            @foreach ($store->product->whereIn('id', $admin['stock']['product']) as $product)
                            <tr>
                                <td>{{$store->name}}</td>
                                <td>{{$product->name}}</td>
                                <td>{{\App\Models\GoodInbound::where('product_id', '=', $product->id)->whereMonth('date_in', $admin['popular']['month'])->whereYear('date_in', $admin['yearly']['year'])->sum('qty')}}</td>
                                <td>{{\App\Models\TransactionDetail::where('product_id', '=', $product->id)->whereMonth('created_at', $admin['popular']['month'])->whereYear('created_at', $admin['yearly']['year'])->sum('qty')}}</td>
                                <td>{{$product->inbound->sum('qty') - $product->transactionDetail->sum('qty')}}</td>
                                <td>Rp{{number_format(\App\Models\TransactionDetail::where('product_id', '=', $product->id)->whereMonth('created_at', $admin['popular']['month'])->whereYear('created_at', $admin['yearly']['year'])->sum('sub_total'), 2, ',', '.')}}</td>
                            </tr>
                            @endforeach
                        @endforeach
                        {{-- @foreach ($admin['stock']['store'] as $store)
                            <tr>
                                <td rowspan="{{count($store->product->whereIn('id', $admin['stock']['product']))}}">{{$store->name}}</td>
                                @foreach ($store->product->whereIn('id', $admin['stock']['product']) as $product)
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->inbound->sum('qty')}}</td>
                                    <td>{{$product->transactionDetail->sum('qty')}}</td>
                                    <td>{{$product->inbound->sum('qty') - $product->transactionDetail->sum('qty')}}</td>
                                </tr>
                                @endforeach
                        @endforeach --}}
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Cabang</th>
                            <th>Produk</th>
                            <th>Jumlah Masuk</th>
                            <th>Jumlah Terjual</th>
                            <th>Jumlah Selisih</th>
                            <th>Jumlah Pendapatan</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
