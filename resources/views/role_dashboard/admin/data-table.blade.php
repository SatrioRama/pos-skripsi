@php
    $total_monthly = 0;
@endphp
<div class="row mt-3">
    <div class="col-md-12">
        <div class="card border-danger col-md-12">
            <div class="card-header text-center">Data Table Transaksi Bulan {{Carbon\Carbon::parse(request('show_dashboard_date', date('Y-m-d')))->format('M Y')}}</div>
            <div class="card-body">
                <table id="data-table" class="display nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-center">Nomor</th>
                            <th class="text-center">Tanggal</th>
                            @foreach (App\Models\StoreBranch::whereIn('id', $admin['store_branch'])->get() as $store)
                                <th class="text-center">{{$store->name}}</th>
                            @endforeach
                            <th class="text-center">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($admin['monthly'] as $key => $date)
                        @php
                            $total_daily = 0;
                        @endphp
                        <tr>
                            <td class="text-center">{{$key+1}}</td>
                            <td class="text-center">{{$date}}</td>
                            @foreach (App\Models\StoreBranch::whereIn('id', $admin['store_branch'])->get() as $store)
                                @php
                                    $data = App\Models\Transaction::where('store_branch_id', '=', $store->id)->whereDate('created_at', date('Y-m-d', strtotime($date)))->sum('total_amount');
                                    $total_daily += $data;
                                @endphp
                                    <td class="text-center" onclick="detail({{ $store->id }}, this.getAttribute('date'))" date="{{date('Y-m-d', strtotime($date))}}">Rp{{number_format($data, 2, ',', '.')}}</td>
                            @endforeach
                            <td class="text-center"><strong>Rp{{number_format($total_daily, 2, ',', '.')}}</strong></td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="text-center" colspan="2">Total</th>
                            @foreach (App\Models\StoreBranch::whereIn('id', $admin['store_branch'])->get() as $store)
                                @php
                                    $data = App\Models\Transaction::where('store_branch_id', '=', $store->id)->whereMonth('created_at', date('m', strtotime($admin['monthly'][0])))->whereYear('created_at', $admin['yearly']['year'])->sum('total_amount');
                                    $total_monthly += $data;
                                @endphp
                                    <th class="text-center">Rp{{number_format($data, 2, ',', '.')}}</th>
                            @endforeach
                                <th class="text-center">Rp{{number_format($total_monthly, 2, ',', '.')}}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
