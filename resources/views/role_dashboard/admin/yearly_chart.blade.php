<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link active border-info border-bottom-0" aria-current="page" href="{{backpack_url('dashboard')}}" id="yearly">Yearly</a>
            </li>
            <li class="nav-item">
              <a class="nav-link border-warning border-bottom-0" aria-current="page" href="{{backpack_url('dashboard')}}" id="monthly">Monthly</a>
            </li>
            <li class="nav-item">
              <a class="nav-link border-danger border-bottom-0" aria-current="page" href="{{backpack_url('dashboard')}}" id="data-stock">Data Table</a>
            </li>
            <li class="nav-item">
              <a class="nav-link border-success border-bottom-0" aria-current="page" href="{{backpack_url('dashboard')}}" id="popular">Popular</a>
            </li>
        </ul>
        <div class="border border-info">
            @php
            Widget::add([
                'type'    => 'div',
                'class'   => 'row',
                'content' => [ // widgets
                    ['type'       => 'chart',
                    'controller' => \App\Http\Controllers\Admin\Charts\AdminYearlyChartController::class,
                    'wrapper' => ['class' => 'col text-center'],
                    'content' => ['header' => 'Transaksi Tahun '.Carbon\Carbon::parse(request('show_dashboard_date', date('Y-m-d')))->format('Y')],
                    ]
                ]
            ])->section('after_content');
            @endphp
        </div>
    </div>
</div>
