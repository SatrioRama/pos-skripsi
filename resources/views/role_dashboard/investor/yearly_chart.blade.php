<div class="row">
    <div class="col-md-12">
        @php
        Widget::add([
            'type'    => 'div',
            'class'   => 'row',
            'content' => [ // widgets
                ['type'       => 'chart',
                'controller' => \App\Http\Controllers\Admin\Charts\InvestorYearlyChartController::class,
                'wrapper' => ['class' => 'col text-center'],
                'content' => ['header' => 'Transaksi Tahun '.Carbon\Carbon::parse(request('show_dashboard_date', date('Y-m-d')))->format('Y')],
                ]
            ]
        ])->section('before_content');
        @endphp
    </div>
</div>
