<div wire:poll.1000ms>
    @if (backpack_user()->hasRole('operator'))
        @if (!empty($times) && $times->end_at > date(\Carbon\Carbon::now()))
            <div class="card bg-warning">
                <div class="card-body justify-content-center">
                    <center><a class="btn btn-warning"><h5 class="justify-content"><strong>{{$vehicle->name}}</strong></h5></a></center>
                    <center><h6 class="justify-content-center"><strong>{{$diff_times}}</strong></h6></center>
                    <center><h6 class="justify-content-center"><strong>In Use</strong></h6></center>
                </div>
            </div>
        @elseif (!empty($times) && $times->end_at < date(\Carbon\Carbon::now()))
            <div class="card bg-danger">
                <div class="card-body justify-content-center">
                    <center><a wire:click="changeStatus({{$times->id}})" class="btn btn-danger stretched-link"><h5 class="justify-content"><strong>{{$vehicle->name}}</strong></h5></a></center>
                    <center><h6 class="justify-content-center"><strong>{{$diff_times}}</strong></h6></center>
                    <center><h6 class="justify-content-center"><strong>In Use</strong></h6></center>
                </div>
            </div>
        @else
            <div class="card bg-primary">
                <div class="card-body justify-content-center">
                    <center><a wire:click="addTime({{$vehicle->id}})" class="btn btn-primary stretched-link"><h5 class="justify-content"><strong>{{$vehicle->name}}</strong></h5></a></center>
                    <center><h6 class="justify-content-center"><strong>{{$diff_times}}</strong></h6></center>
                    <center><h6 class="justify-content-center"><strong>Ready</strong></h6></center>
                </div>
            </div>
        @endif
    @elseif (backpack_user()->hasRole('kasir'))
        @if (!empty($times) && $times->end_at > date(\Carbon\Carbon::now()))
            <div class="card bg-warning">
                <div class="card-body justify-content-center">
                    <center><a class="btn btn-warning"><h5 class="justify-content"><strong>{{$vehicle->name}}</strong></h5></a></center>
                    <center><h6 class="justify-content-center"><strong>{{$diff_times}}</strong></h6></center>
                    <center><h6 class="justify-content-center"><strong>In Use</strong></h6></center>
                </div>
            </div>
        @elseif (!empty($times) && $times->end_at < date(\Carbon\Carbon::now()))
            <div class="card bg-danger">
                <div class="card-body justify-content-center">
                    <center><a class="btn btn-danger stretched-link"><h5 class="justify-content"><strong>{{$vehicle->name}}</strong></h5></a></center>
                    <center><h6 class="justify-content-center"><strong>{{$diff_times}}</strong></h6></center>
                    <center><h6 class="justify-content-center"><strong>In Use</strong></h6></center>
                </div>
            </div>
        @else
            <div class="card bg-primary">
                <div class="card-body justify-content-center">
                    <center><a class="btn btn-primary stretched-link"><h5 class="justify-content"><strong>{{$vehicle->name}}</strong></h5></a></center>
                    <center><h6 class="justify-content-center"><strong>{{$diff_times}}</strong></h6></center>
                    <center><h6 class="justify-content-center"><strong>Ready</strong></h6></center>
                </div>
            </div>
        @endif
    @endif
</div>
