@if (!empty($product->day_active) && in_array($today, $product->day_active))
    <button type="button" wire:click="addToCart({{$product->id}})" class="btn btn-primary col-md-3" style="height: 75px"><strong>{{$product->name}}</strong></button>
@endif
