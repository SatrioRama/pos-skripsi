<div class="input-group mb-3">
    <span class="input-group-text col-md-3" id="voucher_code">Voucher Code</span>
    <input id="voucher_code" type="text" wire.model="voucher_code" wire:change="check($event.target.value)" style="text-transform:uppercase" name="voucher_code" value="{{@$voucher_code}}" class="form-control" aria-label="Sizing example input" aria-describedby="voucher_code">
</div>
