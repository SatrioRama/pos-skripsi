@extends(backpack_view('blank'))

@section('header')
	<section class="container-fluid d-print-none">
    	<a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
		<h2>
	        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
	        <small>{!! $crud->getSubheading() ?? mb_ucfirst(trans('backpack::crud.preview')).' '.$crud->entity_name !!}</small>
	    </h2>
    </section>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row" name="widget_899479116" section="before_content">
            @foreach ($crud->query->where('active', '=', 1)->whereHas('product', function ($query) {
                            return $query->whereHas('storeBranch', function ($query) {
                            return $query->where('store_branches.id', '=', backpack_user()->storeBranch->first()->id);
                        });})->orderBy('name')->get() as $vehicle)
                <div class="col-md-2">
                    @livewire('list-vehicle', ['vehicle' => $vehicle])
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection

@section('after_styles')
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css') }}">
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css') }}">
    @livewireStyles
@endsection

@section('after_scripts')
	<script src="{{ asset('packages/backpack/crud/js/crud.js') }}"></script>
	<script src="{{ asset('packages/backpack/crud/js/show.js') }}"></script>
    @livewireScripts
@endsection
