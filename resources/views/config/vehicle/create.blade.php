<!-- Modal -->
<div class="modal fade" id="addModalVehicle" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="addModalVehicleLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addModalVehicleLabel">Add Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('vehicle.store') }}" method="post" name="form_add_vehicle" id="form_add_vehicle">
                    @csrf
                    <input type="hidden" name="http_referrer" value="{{backpack_url('ticket/'.$crud->entry->id.'/show')}}">
                    <input type="hidden" name="product_id" value="{{ $crud->entry->id }}">

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="name">Name</span>
                        </div>
                        <input type="text" name="name" class="form-control" aria-label="Default" aria-describedby="name">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Description</span>
                        </div>
                        <textarea name="description" class="form-control" aria-label="With textarea"></textarea>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <label class="input-group-text" for="inputGroupSelect01">Active</label>
                        </div>
                        <select name="active" class="custom-select" id="inputGroupSelect01">
                          <option selected value="1">Active</option>
                          <option value="0">Inactive</option>
                        </select>
                    </div>
                    <input type="hidden" name="save_action" value="save_and_back">
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-primary" id="add-buton-out">SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
