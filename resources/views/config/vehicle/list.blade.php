<div class="card no-padding no-border">
    <div class="card-header">
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModalVehicle">
                    <i class="fa fa-plus"></i> ADD VEHICLE
                 </button>
                <br><br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table id="tableVehicle" class="display nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Has Been Used</th>
                            <th class="text-center">Description</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!empty($crud->entry->vehicle) && count($crud->entry->vehicle) > 0)
                            @foreach ($crud->entry->vehicle as $key => $vehicle)
                                <tr>
                                    <td class="text-center">{{$key+1}}</td>
                                    <td class="text-center">{{$vehicle->name}}</td>
                                    <td class="text-center">{{$vehicle->been_used}}</td>
                                    <td class="text-center">{{$vehicle->description}}</td>
                                    <td class="text-center">
                                        <h6 @if ($vehicle->active == 1)
                                            class="badge badge-success"
                                        @else
                                            class="badge badge-danger"
                                        @endif><strong>{{@$active[$vehicle->active]}}</strong></h6><br>
                                    </td>
                                    <td>
                                        <button id="edit" onclick="edit({{ $vehicle->id }}, this.getAttribute('data-id'))" type="button" class="btn btn-warning" style="height: 100%" data-id="{{ route('vehicle.update', $vehicle->id) }}"><i class="las la-pencil-alt"></i></button>
                                        <form id="delete-form{{ $vehicle->id }}" method="POST" action="{{ route('vehicle.destroy', $vehicle->id) }}" class="js-confirm" data-confirm="Apakah anda yakin ingin menghapus data ini?">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-danger" style="height: 100%"><i class="las la-trash-alt"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" class="text-center">No Vehicle Yet</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
