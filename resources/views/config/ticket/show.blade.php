@extends(backpack_view('blank'))

@php
  $defaultBreadcrumbs = [
    trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
    $crud->entity_name_plural => url($crud->route),
    trans('backpack::crud.preview') => false,
  ];

  // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
  $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;

  $dayList = array(
                    'Sunday' => 'Minggu',
                    'Monday' => 'Senin',
                    'Tuesday' => 'Selasa',
                    'Wednesday' => 'Rabu',
                    'Thursday' => 'Kamis',
                    'Friday' => 'Jumat',
                    'Saturday' => 'Sabtu'
                );

  $active = array('Inactive', 'Active');
@endphp

@section('header')
	<section class="container-fluid d-print-none">
    	<a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
		<h2>
	        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
	        <small>{!! $crud->getSubheading() ?? mb_ucfirst(trans('backpack::crud.preview')).' '.$crud->entity_name !!}</small>
	        @if ($crud->hasAccess('list'))
	          <small class=""><a href="{{ url($crud->route) }}" class="font-sm"><i class="la la-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
	        @endif
	    </h2>
    </section>
@endsection

@section('content')

<div class="row">
	<div class="col-md-12">

	<!-- Default box -->
	  <div class="">
	  	@if ($crud->model->translationEnabled())
	    <div class="row">
	    	<div class="col-md-12 mb-2">
				<!-- Change translation button group -->
				<div class="btn-group float-right">
				  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    {{trans('backpack::crud.language')}}: {{ $crud->model->getAvailableLocales()[request()->input('locale')?request()->input('locale'):App::getLocale()] }} &nbsp; <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				  	@foreach ($crud->model->getAvailableLocales() as $key => $locale)
					  	<a class="dropdown-item" href="{{ url($crud->route.'/'.$entry->getKey().'/show') }}?locale={{ $key }}">{{ $locale }}</a>
				  	@endforeach
				  </ul>
				</div>
			</div>
	    </div>
	    @else
	    @endif
	    <div class="card no-padding no-border">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-12">
                        <h6 class="text-left">Code : <strong>{{@$crud->entry->code}}</strong></h6><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="table">
                            <table class="table no-border">
                                <tr>
                                    <td>Name</td>
                                    <td><strong>{{@$crud->entry->name}}</strong></td>
                                </tr>
                                <tr>
                                    <td>Day Active</td>
                                    <td>
                                        @if (!empty(@$crud->entry->day_active))
                                            @foreach (@$crud->entry->day_active as $items)
                                                @if ($loop->last)
                                                    <strong> {{$dayList[$items]}}</strong>
                                                @else
                                                    <strong>{{$dayList[$items]}}, </strong>
                                                @endif
                                            @endforeach
                                        @else
                                            <strong>No Days</strong>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tipe</td>
                                    <td><strong>{{@$crud->entry->type->name}}</strong></td>
                                </tr>
                                <tr>
                                    <td>Timer</td>
                                    <td>
                                        @if (!empty(@$crud->entry->timer))
                                            <strong>{{@$crud->entry->timer}} Menit</strong>
                                        @else
                                            <strong>&infin;</strong>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="table">
                            <table class="table no-border">
                                <tr>
                                    <td>Price</td>
                                    <td><strong>Rp{{number_format(@$crud->entry->price, 2, ',', '.')}}</strong></td>
                                </tr>
                                <tr>
                                    <td>Holiday Price</td>
                                    <td>
                                        @if (!empty(@$crud->entry->holiday_price))
                                            <strong>Rp{{number_format(@$crud->entry->holiday_price, 2, ',', '.')}}</strong>
                                        @else
                                            <strong>Rp{{number_format(@$crud->entry->price, 2, ',', '.')}}</strong>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Promo Price</td>
                                    <td><strong>Rp{{number_format(@$crud->entry->promo_price, 2, ',', '.')}}</strong></td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td><h6 @if ($crud->entry->active == 1)
                                        class="badge badge-success"
                                    @else
                                        class="badge badge-danger"
                                    @endif><strong>{{@$active[$crud->entry->active]}}</strong></h6></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
        @if ($crud->entry->type->flag == \App\Flag::ALC_RACE && !empty($crud->entry->timer))
            @include('config.vehicle.list')
        @endif
	  </div>
	</div>
</div>
@endsection
@include('config.vehicle.create')
@include('config.vehicle.edit')

@section('after_styles')
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css') }}">
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css">
@endsection

@section('after_scripts')
	<script src="{{ asset('packages/backpack/crud/js/crud.js') }}"></script>
	<script src="{{ asset('packages/backpack/crud/js/show.js') }}"></script>
    <script>
        function edit(vehicle_id, data_id) {
            $.ajax({
                type: "post",
                url: "{{ backpack_url('Api/Vehicle') }}",
                data: {
                    vehicle_id: vehicle_id,
                    _token: '{{ csrf_token() }}'
                },
                dataType: "json",
                success: function (response) {
                    if (response.success) {
                        console.log(response.data.id)
                        $('#editModalVehicle').modal('show');
                        $('#form_edit_vehicle').attr('action', data_id);
                        $('input[name=name]').val(response.data.name)
                        $('input[name=been_used]').val(response.data.been_used)
                        $('textarea[name=description]').val(response.data.description)
                        $('select[name=active]').val(response.data.active).trigger('change');
                    }else{
                        swalError({
                            message: response.data.message,
                            response: response.data.error,
                        })
                    }
                }
            });
        }
    </script>
	<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#tableVehicle').DataTable( {
                "pageLength": 5,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );
        } );
    </script>
@endsection
