<?php

namespace App\Http\Livewire;

use Gloudemans\Shoppingcart\Facades\Cart;
use Livewire\Component;

class ListCart extends Component
{
    public $qty;

    protected $listeners = ['cart_updated' => 'render'];

    public function render()
    {
        $carts = Cart::content();
        return view('livewire.list-cart', compact('carts'));
    }

    public function plusToCart($id)
    {
        $find = Cart::get($id);
        Cart::update($id, ['qty' => $find->qty+1]);

        $this->emit('cart_updated');

        session()->flash('success', "Success Add to Cart");
    }

    public function minusToCart($id)
    {
        $find = Cart::get($id);
        Cart::update($id, ['qty' => $find->qty-1]);

        $this->emit('cart_updated');

        session()->flash('success', "Success Reduce from Cart");
    }

    public function destroy($id)
    {
        Cart::remove($id);

        $this->emit('cart_updated');

        session()->flash('success', "Success Remove from Cart");
    }

    public function check($value, $id) {
        $find = Cart::get($id);
        Cart::update($id, ['qty' => $value]);

        $this->emit('cart_updated');

        session()->flash('success', "Success Add to Cart");
    }
}
