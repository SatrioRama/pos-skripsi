<?php

namespace App\Http\Livewire;

use App\Models\Holiday;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Models\Product;
use Carbon\Carbon;
use DateTime;
use Livewire\Component;

class ListProduct extends Component
{
    public $product;

    public function render()
    {
        $product = $this->product;
        $today = Carbon::now()->format('l');

        return view('livewire.list-product', compact(['product', 'today']));
    }

    public function addToCart($product_id)
    {
        $product = Product::find($product_id);
        $find = Cart::content()->where('id', '=', $product_id)->first();
        if ($product->start_date_promo <= Carbon::now()->toDateString() && $product->end_date_promo >= Carbon::now()->toDateString()) {
            $price = (!empty($product->promo_price)) ? $product->promo_price : $product->price;
        } elseif (Carbon::now()->format('l') == "Saturday" || Carbon::now()->format('l') == "Sunday" || Holiday::whereDate('date', date(today()))->first()) {
            $price = (!empty($product->holiday_price) && $product->holiday_price >= 0) ? $product->holiday_price : $product->price;
        } else {
            $price = $product->price;
        }

        if (!empty($find)) {
            Cart::update($find->rowId, ['qty' => $find->qty+1]);
        } else {
            Cart::add($product_id, $product->name, 1, $price);
        }

        $this->emit('cart_updated');

        \Alert::add('success', 'Success add to cart')->flash();
    }
}
