<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ShiftRequest;
use App\Models\Shift;
use App\Models\Transaction;
use App\Models\User;
use App\Status;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class ShiftCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ShiftCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Shift::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/shift');
        CRUD::setEntityNameStrings('shift', 'shifts');
        $this->crud->setShowView('shift.show');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->removeButtons(['create', 'update', 'delete']);
        if (backpack_user()->hasRole('kasir')) {
            $this->crud->addClause('where', 'user_id', '=', backpack_user()->id);
        } elseif (backpack_user()->hasAnyRole(['store manager', 'regional manager', 'investor'])) {
            $this->crud->addClause('whereIn', 'store_branch_id', backpack_user()->storeBranch->pluck('id'));
        }
        $this->crud->addColumn('user_id');
        $this->crud->addColumn([
            // any type of relationship
            'name'         => 'storeBranch', // name of relationship method in the model
            'type'         => 'relationship',
            'label'        => 'Store', // Table column heading
         ]);
        $this->crud->addColumn([
            'name'  => 'open_at', // The db column name
            'label' => 'Mulai', // Table column heading
            'type'  => 'datetime',
            // 'format' => 'l j F Y H:i:s', // use something else than the base.default_datetime_format config value
        ]);
        $this->crud->addColumn([
            'name'  => 'close_at', // The db column name
            'label' => 'Selesai', // Table column heading
            'type'  => 'datetime',
            // 'format' => 'l j F Y H:i:s', // use something else than the base.default_datetime_format config value
        ]);
        $this->crud->addColumn([
            'name'  => 'begining_cash', // The db column name
            'label' => 'Start Cash', // Table column heading
            'type'  => 'number',
            'prefix'        => 'Rp',
            // 'suffix'        => ' EUR',
            'decimals'      => 2,
            'dec_point'     => ',',
            'thousands_sep' => '.'
         ]);
        $this->crud->addColumn([
            'name'  => 'total_transaction', // The db column name
            'label' => 'Total Transaksi', // Table column heading
            'type'  => 'number',
            'prefix'        => 'Rp',
            // 'suffix'        => ' EUR',
            'decimals'      => 2,
            'dec_point'     => ',',
            'thousands_sep' => '.'
         ]);
        $this->crud->addColumn([
            'name'  => 'transaction_cash', // The db column name
            'label' => 'Total Cash', // Table column heading
            'type'  => 'number',
            'prefix'        => 'Rp',
            // 'suffix'        => ' EUR',
            'decimals'      => 2,
            'dec_point'     => ',',
            'thousands_sep' => '.'
         ]);
        $this->crud->addColumn([
            'name'  => 'transaction_edc', // The db column name
            'label' => 'Total EDC', // Table column heading
            'type'  => 'number',
            'prefix'        => 'Rp',
            // 'suffix'        => ' EUR',
            'decimals'      => 2,
            'dec_point'     => ',',
            'thousands_sep' => '.'
         ]);
        $this->crud->addColumn([
            'name'  => 'expected_cash', // The db column name
            'label' => 'Total Setoran', // Table column heading
            'type'  => 'number',
            'prefix'        => 'Rp',
            // 'suffix'        => ' EUR',
            'decimals'      => 2,
            'dec_point'     => ',',
            'thousands_sep' => '.'
         ]);
        $this->crud->addColumn([
            'name'  => 'actual_cash', // The db column name
            'label' => 'Cash di Kasir', // Table column heading
            'type'  => 'number',
            'prefix'        => 'Rp',
            // 'suffix'        => ' EUR',
            'decimals'      => 2,
            'dec_point'     => ',',
            'thousands_sep' => '.'
         ]);
        $this->crud->addColumn([
            'name'  => 'difference', // The db column name
            'label' => 'Selisih', // Table column heading
            'type'  => 'number',
            'prefix'        => 'Rp',
            // 'suffix'        => ' EUR',
            'decimals'      => 2,
            'dec_point'     => ',',
            'thousands_sep' => '.'
         ]);
        $this->crud->addColumn([
            'name'  => 'refund', // The db column name
            'label' => 'Refund', // Table column heading
            'type'  => 'number',
            'prefix'        => 'Rp',
            // 'suffix'        => ' EUR',
            'decimals'      => 2,
            'dec_point'     => ',',
            'thousands_sep' => '.'
         ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ShiftRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store(Request $request)
    {
        $user = User::find($request->user_id);
        $save = new Shift();
        $save->user_id = $request->user_id;
        $save->store_branch_id = backpack_user()->storeBranch->first()->id;
        $save->open_at = Carbon::now();
        $save->begining_cash = $request->begining_cash;
        $save->active = Status::ACTIVE;
        $save->save();

        \Alert::add('success', 'Enjoy Your Work')->flash();
        return redirect(route('transaction.create'));
    }

    public function update($id, Request $request)
    {
        $shift = Shift::find($id);
        $transaction = Transaction::where('shift_id', '=', $id)->where('user_id', '=', backpack_user()->id)->where('store_branch_id', '=', backpack_user()->storeBranch->first()->id)->get();
        $total_refund = Transaction::whereDate('created_at', '=', date(today()))->where('store_branch_id', '=', backpack_user()->storeBranch->first()->id)->sum('refund');
        $total_refund_check = Shift::whereDate('created_at', '=', date(today()))->where('store_branch_id', '=', backpack_user()->storeBranch->first()->id)->sum('refund');
        $sum = $transaction->sum('total_amount');
        $sum_refund = $total_refund - $total_refund_check;

        $shift->close_at = Carbon::now();
        $shift->total_transaction = $sum;
        $shift->transaction_cash = $request->transaction_cash;
        $shift->transaction_edc = $request->transaction_edc;
        $shift->expected_cash = $shift->begining_cash + $request->transaction_cash - ($sum_refund * -1);
        $shift->actual_cash = $request->finish_cash;
        $shift->difference = ($shift->begining_cash + $sum) - ($request->finish_cash + $request->transaction_edc);
        $shift->refund = $sum_refund;
        $shift->active = Status::INACTIVE;
        $shift->update();
        \Alert::add('success', 'Thank You for Your Work')->flash();
        return redirect(backpack_url('shift/'.$id.'/show'));
    }

    public function strukPdf($id)
    {
        $data = Shift::findOrFail($id);

    	return view('shift.output-struk', compact('data'));
    }
}
