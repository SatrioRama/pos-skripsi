<?php

namespace App\Http\Controllers\Admin;

use App\Models\GoodInbound;
use App\Models\Product;
use App\Models\ProductType;
use App\Models\Shift;
use App\Models\StoreBranch;
use App\Models\StoreBranchType;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\Vehicle;
use App\Status;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public $data = []; // the information we send to the view

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware(backpack_middleware());
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request)
    {
        $this->data['title'] = trans('backpack::base.dashboard'); // set the page title
        $this->data['breadcrumbs'] = [
            trans('backpack::crud.admin')     => backpack_url('dashboard'),
            trans('backpack::base.dashboard') => false,
        ];
// dd(backpack_user()->hasRole('investor') , count(backpack_user()->storeProduct));
        if (backpack_user()->hasRole('operator') && count(backpack_user()->storeBranch) == 1) {
            // operator
            $this->operator();
            return view('role_dashboard.operator', $this->data);
        } elseif (backpack_user()->hasRole('kasir') && count(backpack_user()->storeBranch) == 1) {
            // kasir
            $this->kasir();
            return (!empty($this->data['kasir']['shift'])) ? redirect(route('transaction.create')) : view('role_dashboard.kasir', $this->data);
        } elseif (backpack_user()->hasRole('store manager') && count(backpack_user()->storeBranch) >= 1) {
            // store manager
            $this->storeManager();
            return view('role_dashboard.store_manager', $this->data);
        } elseif (backpack_user()->hasRole('regional manager') && count(backpack_user()->storeBranch) >= 1) {
            // regional manager
            $this->regionalManager();
            return view('role_dashboard.regional_manager', $this->data);
        } elseif (backpack_user()->hasRole('investor') && count(backpack_user()->userProduct) >= 1) {
            // investor
            $this->investor();
            return view('role_dashboard.investor', $this->data);
        } elseif (backpack_user()->hasRole('direktur')) {
            // investor
            $this->direktur();
            return view('role_dashboard.direktur', $this->data);
        } elseif (backpack_user()->hasAnyRole(['admin', 'superadmin'])) {
            // investor
            $this->admin();
            return view('role_dashboard.admin', $this->data);
        } elseif(backpack_user()->hasAnyRole(['tax'])){
            $this->pajak();
            return view('role_dashboard.pajak', $this->data);
        }
        else {
            return view('dashboard', $this->data);
        }
    }

    public function operator()
    {
        $vehicle = Vehicle::whereHas('product', function ($query) {
            return $query->whereHas('storeBranch', function ($query) {
            return $query->where('store_branches.id', '=', backpack_user()->storeBranch->first()->id);
        });})->get();

        $this->data['operator'] = $vehicle;
    }

    public function kasir()
    {
        $today = today();
        $dates = [];
        $data = [];
        $tanggal = [];

        for($i=1; $i < $today->daysInMonth + 1; ++$i) {
            $dates[] = \Carbon\Carbon::createFromDate($today->year, $today->month, $i)->format('d-M-Y');
            $tanggal[] = \Carbon\Carbon::createFromDate($today->year, $today->month, $i)->format('d');
        }

        foreach ($dates as $key => $value) {
            $check = Transaction::whereDate('created_at', date('Y-m-d',strtotime($value)))->where('user_id', '=', backpack_user()->id)->get();
            $data[] = $check->sum('total_amount') + $check->sum('refund');
        }

        $today_previous = today()->subMonth();
        $dates_previous = [];
        $data_previous = [];

        for($i=1; $i < $today_previous->daysInMonth + 1; ++$i) {
            $dates_previous[] = \Carbon\Carbon::createFromDate($today_previous->year, $today_previous->month, $i)->format('d-M-Y');
        }

        foreach ($dates_previous as $key => $value_previous) {
            $check_previous = Transaction::whereDate('created_at', date('Y-m-d',strtotime($value_previous)))->where('user_id', '=', backpack_user()->id)->get();
            $data_previous[] = $check_previous->sum('total_amount') + $check_previous->sum('refund');
        }

        $shift = Shift::where('user_id', '=', backpack_user()->id)->where('store_branch_id', '=', backpack_user()->storeBranch->first()->id)->where('active', '=', Status::ACTIVE)->first();

        $this->data['kasir'] = [
            'this_month' => [
                'data' => $data,
                'dates' => $tanggal,
            ],
            'last_month' => [
                'data' => $data_previous,
                'dates' => $dates_previous,
            ],
            'shift' => $shift,
        ];
    }

    public function storeManager()
    {
        $month = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        $month_name = ['Januari', 'Februari', 'Maret', 'April',  'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        $today = Carbon::parse(request('show_dashboard_date', date('Y-m-d')));
        $dates = [];
        for($i=1; $i < $today->daysInMonth + 1; ++$i) {
            $dates[] = \Carbon\Carbon::createFromDate($today->year, $today->month, $i)->format('d-M-Y');
        }

        $this->data['store_manager'] = [
            'monthly' =>  $dates,
            'yearly' =>  [
                'month' => $month,
                'month_name' => $month_name,
                'year' => $today->year,
            ],
        ];
    }

    public function regionalManager()
    {
        $month = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        $month_name = ['Januari', 'Februari', 'Maret', 'April',  'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        $today = Carbon::parse(request('show_dashboard_date', date('Y-m-d')));
        $dates = [];
        for($i=1; $i < $today->daysInMonth + 1; ++$i) {
            $dates[] = \Carbon\Carbon::createFromDate($today->year, $today->month, $i)->format('d-M-Y');
        }

        $storeType_id = backpack_user()->storeBranch;

        $this->data['regional_manager'] = [
            'monthly' =>  $dates,
            'yearly' =>  [
                'month' => $month,
                'month_name' => $month_name,
                'year' => $today->year,
            ],
            'popular' =>  [
                'month' => $today->month,
                'year' => $today->year,
                'store_id' => $storeType_id,
            ],
        ];
    }

    public function investor()
    {
        $month = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        $month_name = ['Januari', 'Februari', 'Maret', 'April',  'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        $today = Carbon::parse(request('show_dashboard_date', date('Y-m-d')));
        $dates = [];
        for($i=1; $i < $today->daysInMonth + 1; ++$i) {
            $dates[] = \Carbon\Carbon::createFromDate($today->year, $today->month, $i)->format('d-M-Y');
        }

        $this->data['investor'] = [
            'monthly' =>  $dates,
            'yearly' =>  [
                'month' => $month,
                'month_name' => $month_name,
                'year' => $today->year,
            ],
            'popular' =>  [
                'month' => $today->month,
                'year' => $today->year,
            ],
        ];
    }

    public function direktur()
    {
        $store_branch_ids = (request('choose_branch') == "all" || request('choose_branch') == NULL) ? StoreBranch::pluck('id') : StoreBranch::where('id', '=', request('choose_branch'))->pluck('id');
        $month = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        $month_name = ['Januari', 'Februari', 'Maret', 'April',  'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        $today = Carbon::parse(request('show_dashboard_date', date('Y-m-d')));
        $dates = [];
        for($i=1; $i < $today->daysInMonth + 1; ++$i) {
            $dates[] = \Carbon\Carbon::createFromDate($today->year, $today->month, $i)->format('d-M-Y');
        }
        $product = (request('choose_branch') == "all" || request('choose_branch') == NULL) ? Product::get() : Product::whereHas('storeBranch', function($q) use($store_branch_ids){
            $q->whereIn('store_branches.id', $store_branch_ids);
        })->get() ;

        $this->data['direktur'] = [
            'store_branch' => $store_branch_ids,
            'monthly' =>  $dates,
            'yearly' =>  [
                'month' => $month,
                'month_name' => $month_name,
                'year' => $today->year,
            ],
            'popular' =>  [
                'month' => $today->month,
                'year' => $today->year,
                'product' => $product,
            ],
        ];
    }

    public function admin()
    {
        $store_branch_ids = (request('choose_branch') == "all" || request('choose_branch') == NULL) ? StoreBranch::pluck('id') : StoreBranch::where('id', '=', request('choose_branch'))->pluck('id');
        $month = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        $month_name = ['Januari', 'Februari', 'Maret', 'April',  'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        $today = Carbon::parse(request('show_dashboard_date', date('Y-m-d')));
        $dates = [];
        for($i=1; $i < $today->daysInMonth + 1; ++$i) {
            $dates[] = \Carbon\Carbon::createFromDate($today->year, $today->month, $i)->format('d-M-Y');
        }
        $product = (request('choose_branch') == "all" || request('choose_branch') == NULL) ? Product::get() : Product::whereHas('storeBranch', function($q) use($store_branch_ids){
            $q->whereIn('store_branches.id', $store_branch_ids);
        })->get() ;

        $good_receive = GoodInbound::distinct('store_branch_id')->pluck('store_branch_id')->toArray();
        $product_receive = GoodInbound::distinct('product_id')->pluck('product_id')->toArray();
        $store_product = (request('choose_branch') == "all" || request('choose_branch') == NULL) ? StoreBranch::whereIn('id', $good_receive)->get() : StoreBranch::where('id', '=', request('choose_branch'))->get();

        $this->data['admin'] = [
            'stock' => [
                'store' => $store_product,
                'product' => $product_receive,
            ],
            'store_branch' => $store_branch_ids,
            'monthly' =>  $dates,
            'yearly' =>  [
                'month' => $month,
                'month_name' => $month_name,
                'year' => $today->year,
            ],
            'popular' =>  [
                'month' => $today->month,
                'year' => $today->year,
                'product' => $product,
            ],
        ];
    }

    public function pajak()
    {
        $month = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        $month_name = ['Januari', 'Februari', 'Maret', 'April',  'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        $today = Carbon::parse(request('show_dashboard_date', date('Y-m-d')));
        $dates = [];
        for($i=1; $i < $today->daysInMonth + 1; ++$i) {
            $dates[] = \Carbon\Carbon::createFromDate($today->year, $today->month, $i)->format('d-M-Y');
        }

        $this->data['store_manager'] = [
            'monthly' =>  $dates,
            'yearly' =>  [
                'month' => $month,
                'month_name' => $month_name,
                'year' => $today->year,
            ],
        ];
    }

    public function details(Request $request)
    {
        $transaction = TransactionDetail::whereDate('created_at', date('Y-m-d', strtotime($request->date)))->whereHas('transaction', function($q) use($request) {
            $q->where('store_branch_id', '=', $request->store_id);
        });
        $product_name = [];
        $total_sold = [];
        $total_sold_sum = 0;
        $income = [];
        $income_sum = 0;
        foreach ($transaction->groupBy('product_id')->pluck('product_id') as $id) {
            $detail = TransactionDetail::whereDate('created_at', date('Y-m-d', strtotime($request->date)))->whereHas('transaction', function($q) use($request) {
                $q->where('store_branch_id', '=', $request->store_id);
            });
            $product_name[] = Product::find($id)->name;
            $total_sold[] = $detail->where('product_id', '=', $id)->sum('qty');
            $total_sold_sum += $detail->where('product_id', '=', $id)->sum('qty');
            $income[] = 'Rp'.number_format($detail->where('product_id', '=', $id)->sum('sub_total'), 2, ',', '.');
            $income_sum += $detail->where('product_id', '=', $id)->sum('sub_total');
        }
        $data = [
            'store_branch' => StoreBranch::find($request->store_id)->name,
            'date_label' => date('d-m-Y', strtotime($request->date)),
            'product_name' => $product_name,
            'total_sold' => $total_sold,
            'total_sold_sum' => (string)$total_sold_sum,
            'income' => $income,
            'income_sum' => 'Rp'.number_format($income_sum, 2, ',', '.'),
        ];
        if ($transaction == null) {
            $return = array (
                'code' => 404,
                'error' => true,
                'message' => 'Data Tidak Ditemukan',
            );
        }else{
            $return = array (
                'code' => 200,
                'success' => true,
                'data' => $data,
                'message' => 'Data Ditemukan',
            );
        }
        return $return;
    }
}
