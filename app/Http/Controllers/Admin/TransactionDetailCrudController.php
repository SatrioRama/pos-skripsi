<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TransactionDetailRequest;
use App\Models\Product;
use App\Models\ReportTransaction;
use App\Models\TransactionDetail;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TransactionDetailCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TransactionDetailCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\TransactionDetail::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/transaction-detail');
        CRUD::setEntityNameStrings('transaction detail', 'transaction details');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->removeButtons(['create', 'show', 'update', 'delete']);
        if (backpack_user()->hasRole('kasir')) {
            $this->crud->addClause('whereHas', 'transaction', function($query) {
                $query->where('user_id', '=', backpack_user()->id);
               });
        }elseif (backpack_user()->hasAnyRole(['store manager', 'regional manager'])) {
            $this->crud->addClause('whereHas', 'transaction', function($query) {
                $query->whereIn('store_branch_id', backpack_user()->storeBranch->pluck('id'));
            });
        }elseif (backpack_user()->hasRole('investor')) {
            $this->crud->addClause('whereIn', 'product_id', backpack_user()->userProduct->pluck('id'));
        }

        $this->crud->enableExportButtons();

        $this->crud->addFilter([
        'name'  => 'transaction_number',
        'type'  => 'select2_multiple',
        'label' => 'Filter Transaction Number',
        ], function() {
            return ReportTransaction::whereIn('id', TransactionDetail::pluck('transaction_id')->toArray())->pluck('transaction_number', 'id')->toArray();
        }, function($values) { // if the filter is active
            $this->crud->addClause('whereIn', 'transaction_id', json_decode($values));
        });
        $this->crud->addFilter([
            'type'  => 'date_range',
            'name'  => 'from_to',
            'label' => 'Date Range'
          ],
          false,
          function ($value) { // if the filter is active, apply these constraints
            $dates = json_decode($value);
            $this->crud->addClause('whereHas', 'transaction', function($query) use($dates) {
                $query->where('created_at', '>=', $dates->from)->where('created_at', '<=', $dates->to . ' 23:59:59');
               });
          });
        $this->crud->addFilter([
        'name'  => 'product',
        'type'  => 'select2_multiple',
        'label' => 'Filter Product',
        ], function() {
            return Product::whereIn('id', TransactionDetail::pluck('product_id')->toArray())->pluck('name', 'id')->toArray();
        }, function($values) { // if the filter is active
            $this->crud->addClause('whereIn', 'product_id', json_decode($values));
        });
        $this->crud->addFilter([
            'name'       => 'qty',
            'type'       => 'range',
            'label'      => 'Filter Quantity',
            'label_from' => 'Min',
            'label_to'   => 'Max'
          ],
          false,
          function($value) { // if the filter is active
              $range = json_decode($value);
              if ($range->from) {
                  $this->crud->addClause('where', 'qty', '>=', (float) $range->from);
              }
              if ($range->to) {
                  $this->crud->addClause('where', 'qty', '<=', (float) $range->to);
              }
          });
        $this->crud->addFilter([
            'name'       => 'subtotal',
            'type'       => 'range',
            'label'      => 'Filter Subtotal',
            'label_from' => 'Min',
            'label_to'   => 'Max'
          ],
          false,
          function($value) { // if the filter is active
              $range = json_decode($value);
              if ($range->from) {
                  $this->crud->addClause('where', 'sub_total', '>=', (float) $range->from);
              }
              if ($range->to) {
                  $this->crud->addClause('where', 'sub_total', '<=', (float) $range->to);
              }
          });

        $this->crud->addColumn([
            'name'      => 'row_number',
            'type'      => 'row_number',
            'label'     => 'Number',
            'orderable' => false,
        ])->makeFirstColumn();
        $this->crud->addColumn([
            'name'      => 'transaction_number',
            'type'      => 'text',
            'label'     => 'Transaction Number',
            'wrapper'   => [
                // 'element' => 'a', // the element will default to "a" so you can skip it here
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url('report-transaction/'.$entry->transaction->id.'/show');
                },
                // 'target' => '_blank',
                // 'class' => 'some-class',
            ],
        ]);
        $this->crud->addColumn([
            // 1-n relationship
            'label'     => 'Product', // Table column heading
            'type'      => 'select',
            'name'      => 'product_id', // the column that contains the ID of that connected entity;
            'entity'    => 'product', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => "App\Models\Product", // foreign key model
        ]);
        $this->crud->addColumn([
            'name'      => 'qty',
            'type'      => 'number',
            'label'     => 'Quantity',
        ]);
        $this->crud->addColumn([
            'name'     => 'price',
            'label'    => 'Price',
            'type'     => 'closure',
            'function' => function($entry) {
                return 'Rp'.number_format($entry->price, 2, ',', '.');
            }
        ]);
        $this->crud->addColumn([
            'name'     => 'sub_total',
            'label'    => 'Subtotal',
            'type'     => 'closure',
            'function' => function($entry) {
                return 'Rp'.number_format($entry->sub_total, 2, ',', '.');
            }
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(TransactionDetailRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
