<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TransactionRequest;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\User;
use App\Models\Voucher;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Database\Eloquent\Builder;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Class TransactionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TransactionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Transaction::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/transaction');
        CRUD::setEntityNameStrings('transaction', 'transactions');
        $this->crud->setCreateView('transaction.create');
        $this->crud->setEditView('transaction.edit');
        $this->crud->setShowView('transaction.show');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        if (!backpack_user()->hasAnyRole(['admin', 'superadmin'])) {
            $this->crud->removeButton('delete');
        }
        $this->crud->removeButtons(['create', 'show']);
        if (!backpack_user()->hasAnyRole(['store manager', 'regional manager', 'admin', 'superadmin'])) {
            $this->crud->removeButton('update');
        }
        if (backpack_user()->hasAnyRole(['kasir', 'store manager', 'regional manager'])) {
            $this->crud->addButtonFromView('line', 'update', 'refund', 'beginning');
            $this->crud->addClause('whereIn', 'store_branch_id', backpack_user()->storeBranch->pluck('id'));
            $this->crud->addClause('whereDate', 'created_at', '=', date(today()));
        }

        $this->crud->addFilter([
            'type'  => 'text',
            'name'  => 'transaction_number',
            'label' => 'Transaction Number'
          ],
          false,
          function($value) { // if the filter is active
            $this->crud->addClause('where', 'transaction_number', 'LIKE', "%$value%");
          });

        $this->crud->addButtonFromView('line', 'preview', 'transaction', 'beginning');

        $this->crud->addColumn([
            'name'      => 'row_number',
            'type'      => 'row_number',
            'label'     => 'Number',
            'orderable' => false,
        ])->makeFirstColumn();
        $this->crud->addColumn([
            'name'      => 'transaction_number',
            'type'      => 'text',
            'label'     => 'Transaction Number',
        ]);
        $this->crud->addColumn([
            'name'      => 'created_at',
            'type'      => 'datetime',
            'format'    => 'H:mm:ss, D MMM YYYY',
            'label'     => 'Waktu Transaksi',
        ]);
        $this->crud->addColumn([
            'name'     => 'total_amount',
            'label'    => 'Total Amount',
            'type'     => 'closure',
            'function' => function($entry) {
                return 'Rp'.number_format($entry->total_amount, 2, ',', '.');
            }
        ]);
        $this->crud->addColumn([
            'name'     => 'cash',
            'label'    => 'Cash',
            'type'     => 'closure',
            'function' => function($entry) {
                return 'Rp'.number_format($entry->cash, 2, ',', '.');
            }
        ]);
        $this->crud->addColumn([
            'name'     => 'change',
            'label'    => 'Change',
            'type'     => 'closure',
            'function' => function($entry) {
                return 'Rp'.number_format($entry->change, 2, ',', '.');
            }
        ]);
        $this->crud->addColumn([
            'name'     => 'refund',
            'label'    => 'Refund',
            'type'     => 'closure',
            'function' => function($entry) {
                return 'Rp'.number_format($entry->refund, 2, ',', '.');
            }
        ]);
        $this->crud->addColumn([
            // 1-n relationship
            'label'     => 'Payment Method', // Table column heading
            'type'      => 'select',
            'name'      => 'payment_method', // the column that contains the ID of that connected entity;
            'entity'    => 'paymentMethod', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => "App\Models\PaymentMethod", // foreign key model
        ]);
        $this->crud->addColumn([
            // 1-n relationship
            'label'     => 'Kasir', // Table column heading
            'type'      => 'select',
            'name'      => 'user_id', // the column that contains the ID of that connected entity;
            'entity'    => 'user', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => "App\Models\User", // foreign key model
        ]);
        $this->crud->addColumn([
            // 1-n relationship
            'label'     => 'Store', // Table column heading
            'type'      => 'select',
            'name'      => 'store_branch_id', // the column that contains the ID of that connected entity;
            'entity'    => 'storebranch', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => "App\Models\StoreBranch", // foreign key model
        ]);
        $this->crud->addColumn([
            'name'     => 'detail',
            'label'    => 'Item',
            'type'     => 'closure',
            'function' => function($entry) {
                return count($entry->details);
            },
            'wrapper'   => [
                // 'element' => 'a', // the element will default to "a" so you can skip it here
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url('transaction-detail');
                },
                // 'target' => '_blank',
                // 'class' => 'some-class',
            ],
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(TransactionRequest::class);

        $this->crud->addField([
            'name' => 'shift_id',
            'type' => 'hidden',
        ]);
        $this->crud->addField([
            'name' => 'transaction_number',
            'type' => 'hidden',
        ]);
        $this->crud->addField([
            'name' => 'payment_method',
            'type' => 'hidden',
        ]);
        $this->crud->addField([
            'name' => 'total_amount',
            'type' => 'hidden',
        ]);
        $this->crud->addField([
            'name' => 'cash',
            'type' => 'hidden',
        ]);
        $this->crud->addField([
            'name' => 'change',
            'type' => 'hidden',
        ]);
        $this->crud->addField([
            'name' => 'ticket_type',
            'type' => 'hidden',
        ]);
        $this->crud->addField([
            'name' => 'cash_back',
            'type' => 'hidden',
        ]);
        $this->crud->addField([
            'name' => 'user_id',
            'type' => 'hidden',
        ]);
        $this->crud->addField([
            'name' => 'store_branch_id',
            'type' => 'hidden',
        ]);
        $this->crud->addField([
            'name' => 'refund',
            'type' => 'number',
        ]);
        $this->crud->addField([
            'name' => 'description',
            'type' => 'textarea',
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store(Request $request)
    {
        if ($request->total_amount-$request->discount > $request->cash || count(Cart::content()) == 0) {
            \Alert::add('danger', 'Total Amount More Than Cash')->flash();
            return redirect()->back();
        }
        $find_voucher = Voucher::where('voucher_code', '=', $request->voucher_code)->where('active', '=', true)->first();
        $transaction = Transaction::whereMonth('created_at', '=', date('m'))->withTrashed()->count('id');
        $transaction_number = date('m').date('d').str_pad($transaction+1, 5, "0", STR_PAD_LEFT).date('Y');
        $save = new Transaction();
        $save->shift_id = $request->shift_id;
        $save->transaction_number = $transaction_number;
        $save->total = $request->total_amount;
        $save->discount = $request->discount;
        if ($request->total_amount-$request->discount < 0) {
            $should_discount = $request->total_amount;
        } else {
            $should_discount = $request->discount;
        }
        $save->total_amount = $request->total_amount-$should_discount;
        $save->cash = $request->cash;
        $save->payment_method = $request->payment_method;
        $save->approval_code = strtoupper($request->approval_code);
        if (!empty($find_voucher)) {
            $save->voucher_id = $find_voucher->id;
            $update_voucher = Voucher::find($find_voucher->id);
            $update_voucher->active = false;
            $update_voucher->update();
        }
        $save->change = $request->cash - ($request->total_amount - $should_discount);
        $save->user_id = $request->user_id;
        $save->store_branch_id = $request->store_branch_id;
        $save->save();
        $transaction_id = Transaction::where('transaction_number', '=', $transaction_number)->where('shift_id', '=', $request->shift_id)->where('store_branch_id', '=', $request->store_branch_id)->orderBy('created_at', 'DESC')->first()->id;
        foreach (Cart::content() as $key => $product) {
            $detail = new TransactionDetail();
            $detail->transaction_id = $transaction_id;
            $detail->transaction_number = $transaction_number;
            $detail->product_id = $product->id;
            $detail->qty = $product->qty;
            $detail->price = $product->price;
            $detail->sub_total = $product->subtotal;
            $detail->save();
        }
        Cart::destroy();
        \Alert::add('success', 'Transaction Has Been Created')->flash();
        return redirect(route('transaction.show', $transaction_id));
    }

    public function update($id, Request $request)
    {
        if (empty($request->description)) {
            \Alert::add('danger', 'Please fill the description')->flash();
            return redirect()->back();
        }
        $store_manager = User::whereHas('storeBranch', function (Builder $query) use ($id) {
            $query->where('store_branches.id', '=', Transaction::find($id)->store_branch_id);
        })->where('username', '=', $request->username_store_manager)->first();
        if (empty($store_manager) || Hash::check($request->password_store_manager, $store_manager->password) == false) {
            \Alert::add('danger', 'Username or Password store manager is wrong')->flash();
            return redirect()->back();
        }
        foreach ($request->id as $key => $value) {
            $tdetail = TransactionDetail::find($value);
            $sumQty = TransactionDetail::where('transaction_id', '=', $id)->where('product_id', '=', $tdetail->product_id)->sum('qty');
            if ($sumQty-$request->qty[$key] < 0) {
                \Alert::add('danger', 'Quantity refund exceeds for '.$tdetail->product->name)->flash();
                return redirect()->back();
            }
        }
        foreach ($request->id as $key => $value) {
            if (!empty($request->qty[$key])) {
                $detail = TransactionDetail::find($value);
                $save = new TransactionDetail();
                $save->transaction_id = $detail->transaction_id;
                $save->transaction_number = $detail->transaction_number;
                $save->product_id = $detail->product_id;
                $save->qty = -$request->qty[$key];
                $save->price = $detail->price;
                $save->sub_total = $detail->price * -$request->qty[$key];
                $save->save();
            }
        }
        $transaction = Transaction::find($id);
        $transaction->refund = $transaction->details->where('sub_total', '<=', 0)->sum('sub_total');
        $transaction->total = $transaction->details->sum('sub_total');
        if (!empty($transaction->voucher_id)) {
            $find_voucher = Voucher::find($transaction->voucher_id);
            $find_details_voucher = TransactionDetail::whereIn('id', $find_voucher->product->pluck('id'))->first();
            $sub_total_product_discount = TransactionDetail::where('transaction_id', '=', $id)->where('product_id', '=', $find_details_voucher->product_id)->sum('sub_total');
            if ($find_voucher->disc > $sub_total_product_discount) {
                $transaction->discount = $sub_total_product_discount;
                $should_discount = $sub_total_product_discount;
            } else {
                $transaction->discount = $find_voucher->disc;
                $should_discount = $find_voucher->disc;
            }
        } else {
            $should_discount = 0;
        }
        $transaction->total_amount = $transaction->details->sum('sub_total') - $should_discount;
        $transaction->change = $transaction->cash - ($transaction->details->sum('sub_total') - $should_discount);
        $transaction->description = $request->description;
        $transaction->struk_print = false;
        $transaction->ticket_print = true;
        $transaction->kasir_refund_id = backpack_user()->id;
        $transaction->store_refund_id = $store_manager->id;
        $transaction->update();

        \Alert::add('success', 'Transaction Has Been Updated')->flash();
        return redirect(backpack_url('transaction/'.$id.'/show'));
    }

    public function strukPdf($id)
    {
        $data = Transaction::findOrFail($id);
        $data->struk_print = true;
        $data->update();

    	return view('transaction.output-struk', compact('data'));
    }

    public function ticketPdf($id)
    {
        $data = Transaction::findOrFail($id);
        $data->ticket_print = true;
        $data->update();

    	return view('transaction.output-ticket', compact('data'));
    }

    public function preview($id)
    {
        $crud = $this;
        $data = Transaction::find($id);
        return view('transaction.preview', compact(['crud', 'data']));
    }

    public function destroy($id)
    {
        $data = Transaction::findOrFail($id);
        foreach ($data->details as $detail) {
            $detail->delete();
        }

        $id = $this->crud->getCurrentEntryId() ?? $id;

        return $this->crud->delete($id);
    }
}
