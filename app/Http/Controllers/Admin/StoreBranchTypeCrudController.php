<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreBranchTypeRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class StoreBranchTypeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class StoreBranchTypeCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\StoreBranchType::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/storebranchtype');
        CRUD::setEntityNameStrings('Store Branch Type', 'Store Branch Types');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name'      => 'row_number',
            'type'      => 'row_number',
            'label'     => 'Nomor',
            'orderable' => false,
        ])->makeFirstColumn();
        CRUD::setFromDb(); // columns
        $this->crud->addColumn([
            // relationship count
            'name'      => 'storeBranch', // name of relationship method in the model
            'type'      => 'relationship_count',
            'label'     => 'Store', // Table column heading
            'wrapper'   => [
                // 'element' => 'a', // the element will default to "a" so you can skip it here
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url('storebranch');
                },
                // 'target' => '_blank',
                // 'class' => 'some-class',
            ],
            // OPTIONAL
            // 'suffix' => ' tags', // to show "123 tags" instead of "123 items"
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(StoreBranchTypeRequest::class);

        CRUD::setFromDb(); // fields
        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Product Type",
            'type'      => 'select2_multiple',
            'name'      => 'productType', // the method that defines the relationship in your Model

            // optional
            'entity'    => 'productType', // the method that defines the relationship in your Model
            'model'     => "App\Models\ProductType", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            'select_all' => true, // show Select All and Clear buttons?
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->addField([
            'label'     => "Name",
            'type'      => 'text',
            'name'      => 'name', // the method that defines the relationship in your Model
            'attributes' => [
                'readonly'    => 'readonly',
              ],
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Product Type",
            'type'      => 'select2_multiple',
            'name'      => 'productType', // the method that defines the relationship in your Model

            // optional
            'entity'    => 'productType', // the method that defines the relationship in your Model
            'model'     => "App\Models\ProductType", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            'select_all' => true, // show Select All and Clear buttons?
        ]);
    }
}
