<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\GoodInboundRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class GoodInboundCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class GoodInboundCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\GoodInbound::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/good-inbound');
        CRUD::setEntityNameStrings('good inbound', 'good inbounds');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn('date_in');
        $this->crud->addColumn([
            // any type of relationship
            'name'         => 'storeBranch', // name of relationship method in the model
            'type'         => 'relationship',
            'label'        => 'Store Branch', // Table column heading
            // OPTIONAL
            // 'entity'    => 'tags', // the method that defines the relationship in your Model
            // 'attribute' => 'name', // foreign key attribute that is shown to user
            // 'model'     => App\Models\Category::class, // foreign key model
         ]);
        $this->crud->addColumns(['product_id','qty']);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(GoodInboundRequest::class);

        $this->crud->addField([   // date_picker
            'name'  => 'date_in',
            'type'  => 'date_picker',
            'label' => 'Date In',

            // optional:
            'date_picker_options' => [
               'todayBtn' => 'linked',
               'format'   => 'dd-mm-yyyy',
            //    'language' => 'fr'
            ],
        ]);

        $this->crud->addField([  // Select2
            'label'     => "Store Branch",
            'type'      => 'select2',
            'name'      => 'store_branch_id', // the db column for the foreign key

            // optional
            'entity'    => 'storeBranch', // the method that defines the relationship in your Model
            'model'     => "App\Models\StoreBranch", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user

             // also optional
            // 'options'   => (function ($query) {
            //      return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
            //  }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
        ]);

        $this->crud->addField([   // repeatable
            'name'  => 'goods',
            'label' => 'Data Barang',
            'type'  => 'repeatable',
            'fields' => [
                [  // Select2
                    'label'     => "Product",
                    'type'      => 'select2',
                    'name'      => 'product_id', // the db column for the foreign key

                    // optional
                    'entity'    => 'product', // the method that defines the relationship in your Model
                    'model'     => "App\Models\Product", // foreign key model
                    'attribute' => 'nameDetail', // foreign key attribute that is shown to user
                    'wrapper' => ['class' => 'form-group col-md-6'],

                     // also optional
                    // 'options'   => (function ($query) {
                    //      return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
                    //  }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
                ],
                [
                    'name'    => 'qty',
                    'type'    => 'number',
                    'label'   => 'Quantity',
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
            ],

            // optional
            'new_item_label'  => 'Add Product', // customize the text of the button
            'init_rows' => 1, // number of empty rows to be initialized, by default 1
            'min_rows' => 1, // minimum rows allowed, when reached the "delete" buttons will be hidden
            // 'max_rows' => 2, // maximum rows allowed, when reached the "new item" button will be hidden

        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->addField([   // date_picker
            'name'  => 'date_in',
            'type'  => 'date_picker',
            'label' => 'Date In',

            // optional:
            'date_picker_options' => [
               'todayBtn' => 'linked',
               'format'   => 'dd-mm-yyyy',
            //    'language' => 'fr'
            ],
        ]);

        $this->crud->addField([  // Select2
            'label'     => "Store Branch",
            'type'      => 'select2',
            'name'      => 'store_branch_id', // the db column for the foreign key

            // optional
            'entity'    => 'storeBranch', // the method that defines the relationship in your Model
            'model'     => "App\Models\StoreBranch", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user

             // also optional
            // 'options'   => (function ($query) {
            //      return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
            //  }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
        ]);

        $this->crud->addField([  // Select2
            'label'     => "Product",
            'type'      => 'select2',
            'name'      => 'product_id', // the db column for the foreign key

            // optional
            'entity'    => 'product', // the method that defines the relationship in your Model
            'model'     => "App\Models\Product", // foreign key model
            'attribute' => 'nameDetail', // foreign key attribute that is shown to user
            'wrapper' => ['class' => 'form-group col-md-6'],

             // also optional
            // 'options'   => (function ($query) {
            //      return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
            //  }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
        ]);

        $this->crud->addField([
            'name'    => 'qty',
            'type'    => 'number',
            'label'   => 'Quantity',
            'wrapper' => ['class' => 'form-group col-md-6'],
        ]);
    }

    public function store()
    {
        $this->crud->hasAccessOrFail('create');

        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();

        foreach (json_decode($this->crud->getStrippedSaveRequest()['goods']) as $key => $value) {
            $save = [
                "date_in" => $request->date_in,
                "store_branch_id" => $request->store_branch_id,
                "product_id" => $value->product_id,
                "qty" => $value->qty,
            ];
            $item = $this->crud->create($save);
            $this->data['entry'] = $this->crud->entry = $item;
        }
        // insert item in the db

        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($item->getKey());
    }
}
