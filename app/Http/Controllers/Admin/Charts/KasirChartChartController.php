<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Http\Controllers\Admin\AdminController;
use App\Models\Transaction;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

/**
 * Class KasirChartChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class KasirChartChartController extends ChartController
{
    public function setup()
    {
        $db = new AdminController;

        $db->kasir();

        $this->chart = new Chart();

        $this->chart->dataset('This Month', 'line', $db->data['kasir']['this_month']['data'])->color('rgb(77, 189, 116)')->backgroundColor('rgba(77, 189, 116, 0.4)');
        $this->chart->dataset('Last Month', 'line', $db->data['kasir']['last_month']['data'])->color('rgb(96, 92, 168)')->backgroundColor('rgba(96, 92, 168, 0.4)');

        // OPTIONAL
        $this->chart->minimalist(true);
        $this->chart->displayAxes(true);
        $this->chart->displayLegend(true);

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels($db->data['kasir']['this_month']['dates']);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    // public function data()
    // {
    //     $users_created_today = \App\User::whereDate('created_at', today())->count();

    //     $this->chart->dataset('Users Created', 'bar', [
    //                 $users_created_today,
    //             ])
    //         ->color('rgba(205, 32, 31, 1)')
    //         ->backgroundColor('rgba(205, 32, 31, 0.4)');
    // }
}
