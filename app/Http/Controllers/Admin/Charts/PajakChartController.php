<?php

namespace App\Http\Controllers\Admin\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use App\Http\Controllers\Admin\AdminController;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use App\Models\Transaction;
use App\Color;

class PajakChartController extends ChartController
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function setup()
    {
        $db = new AdminController;

        $db->pajak();

        $this->chart = new Chart();
        foreach (backpack_user()->storeBranch as $key => $store) {
            $data = [];
            foreach ($db->data['store_manager']['yearly']['month'] as $key => $month) {
                $data[] = Transaction::where('store_branch_id', '=', $store->id)->whereMonth('created_at', $month)->whereYear('created_at', $db->data['store_manager']['yearly']['year'])->sum('total_amount')/5;
            }
            $warna = Color::hex2rgb($store->background_color);
            $this->chart->dataset($store->name, 'line', $data)->color('rgb('.$warna[0].', '.$warna[1].', '.$warna[2].')')->backgroundColor('rgba('.$warna[0].', '.$warna[1].', '.$warna[2].', 0.4)');
        }

        // OPTIONAL
        $this->chart->minimalist(true);
        $this->chart->displayAxes(true);
        $this->chart->displayLegend(true);

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels($db->data['store_manager']['yearly']['month_name']);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
}
