<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Color;
use App\Http\Controllers\Admin\AdminController;
use App\Models\Product;
use App\Models\TransactionDetail;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

/**
 * Class AdminPopularProductChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AdminPopularProductChartController extends ChartController
{
    public function setup()
    {
        $db = new AdminController;

        $db->admin();

        $this->chart = new Chart();
        foreach ($db->data['admin']['popular']['product'] as $key => $product) {
            $data = [];
            foreach ($db->data['admin']['monthly'] as $key => $date) {
                $sum = TransactionDetail::whereDate('created_at', date('Y-m-d', strtotime($date)))->where('product_id', '=', $product->id)->sum('sub_total');
                $data[] = (int)$sum;
            }
            $color = str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
            $warna = Color::hex2rgb('#'.$color);
            $this->chart->dataset($product->name." - ".$product->storeBranch->first()->name, 'bar', $data)->color('rgb('.$warna[0].', '.$warna[1].', '.$warna[2].')')->backgroundColor('rgba('.$warna[0].', '.$warna[1].', '.$warna[2].', 0.4)');
        }

        // OPTIONAL
        $this->chart->minimalist(true);
        $this->chart->displayAxes(true);
        $this->chart->displayLegend(true);

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels($db->data['admin']['monthly']);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    // public function data()
    // {
    //     $users_created_today = \App\User::whereDate('created_at', today())->count();

    //     $this->chart->dataset('Users Created', 'bar', [
    //                 $users_created_today,
    //             ])
    //         ->color('rgba(205, 32, 31, 1)')
    //         ->backgroundColor('rgba(205, 32, 31, 0.4)');
    // }
}
