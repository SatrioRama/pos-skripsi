<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Color;
use App\Http\Controllers\Admin\AdminController;
use App\Models\StoreBranch;
use App\Models\Transaction;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

/**
 * Class AdminYearlyChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AdminYearlyChartController extends ChartController
{
    public function setup()
    {
        $db = new AdminController;

        $db->admin();

        $this->chart = new Chart();
        foreach (StoreBranch::whereIn('id', $db->data['admin']['store_branch'])->get() as $key => $store) {
            $data = [];
            foreach ($db->data['admin']['yearly']['month'] as $key => $month) {
                $data[] = Transaction::where('store_branch_id', '=', $store->id)->whereMonth('created_at', $month)->whereYear('created_at', $db->data['admin']['yearly']['year'])->sum('total_amount');
            }
            $warna = Color::hex2rgb($store->background_color);
            $this->chart->dataset($store->name, 'bar', $data)->color('rgb('.$warna[0].', '.$warna[1].', '.$warna[2].')')->backgroundColor('rgba('.$warna[0].', '.$warna[1].', '.$warna[2].', 0.4)');
        }

        // OPTIONAL
        $this->chart->minimalist(true);
        $this->chart->displayAxes(true);
        $this->chart->displayLegend(true);

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels($db->data['admin']['yearly']['month_name']);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    // public function data()
    // {
    //     $users_created_today = \App\User::whereDate('created_at', today())->count();

    //     $this->chart->dataset('Users Created', 'bar', [
    //                 $users_created_today,
    //             ])
    //         ->color('rgba(205, 32, 31, 1)')
    //         ->backgroundColor('rgba(205, 32, 31, 0.4)');
    // }
}
