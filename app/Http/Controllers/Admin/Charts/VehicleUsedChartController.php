<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Http\Controllers\Admin\AdminController;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Illuminate\Http\Request;
use Carbon\Carbon;

/**
 * Class VehicleUsedChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class VehicleUsedChartController extends ChartController
{
    public function setup()
    {
        $db = new AdminController;

        $db->operator();

        $this->chart = new Chart();
        $this->chart->load(backpack_url('charts/package-status'));

        $this->chart = new Chart();
        $name = array();
        $used = array();
        foreach ($db->data['operator'] as $key => $vehicle) {
            $name[] = $vehicle->name.' '.$vehicle->been_used;
            $used[] = count($vehicle->used);
        }
        $this->chart->dataset('penggunaan', 'bar', $used)->backgroundColor('#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT));
        $this->chart->labels($name);

        $this->chart->options([
            'tooltip' => [
                'show' => true // or false, depending on what you want.
            ]
        ]);
        $this->chart->displayAxes(true);
        $this->chart->displayLegend(true);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    // public function data()
    // {
    //     $users_created_today = \App\User::whereDate('created_at', today())->count();

    //     $this->chart->dataset('Users Created', 'bar', [
    //                 $users_created_today,
    //             ])
    //         ->color('rgba(205, 32, 31, 1)')
    //         ->backgroundColor('rgba(205, 32, 31, 0.4)');
    // }
}
